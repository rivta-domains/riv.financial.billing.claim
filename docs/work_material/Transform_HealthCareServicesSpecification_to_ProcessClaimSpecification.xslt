<?xml version="1.0" encoding="UTF-8"?>
<!--
Apache License
                           Version 2.0, January 2004
                        http://www.apache.org/licenses/

   TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION

   1. Definitions.

      "License" shall mean the terms and conditions for use, reproduction,
      and distribution as defined by Sections 1 through 9 of this document.

      "Licensor" shall mean the copyright owner or entity authorized by
      the copyright owner that is granting the License.

      "Legal Entity" shall mean the union of the acting entity and all
      other entities that control, are controlled by, or are under common
      control with that entity. For the purposes of this definition,
      "control" means (i) the power, direct or indirect, to cause the
      direction or management of such entity, whether by contract or
      otherwise, or (ii) ownership of fifty percent (50%) or more of the
      outstanding shares, or (iii) beneficial ownership of such entity.

      "You" (or "Your") shall mean an individual or Legal Entity
      exercising permissions granted by this License.

      "Source" form shall mean the preferred form for making modifications,
      including but not limited to software source code, documentation
      source, and configuration files.

      "Object" form shall mean any form resulting from mechanical
      transformation or translation of a Source form, including but
      not limited to compiled object code, generated documentation,
      and conversions to other media types.

      "Work" shall mean the work of authorship, whether in Source or
      Object form, made available under the License, as indicated by a
      copyright notice that is included in or attached to the work
      (an example is provided in the Appendix below).

      "Derivative Works" shall mean any work, whether in Source or Object
      form, that is based on (or derived from) the Work and for which the
      editorial revisions, annotations, elaborations, or other modifications
      represent, as a whole, an original work of authorship. For the purposes
      of this License, Derivative Works shall not include works that remain
      separable from, or merely link (or bind by name) to the interfaces of,
      the Work and Derivative Works thereof.

      "Contribution" shall mean any work of authorship, including
      the original version of the Work and any modifications or additions
      to that Work or Derivative Works thereof, that is intentionally
      submitted to Licensor for inclusion in the Work by the copyright owner
      or by an individual or Legal Entity authorized to submit on behalf of
      the copyright owner. For the purposes of this definition, "submitted"
      means any form of electronic, verbal, or written communication sent
      to the Licensor or its representatives, including but not limited to
      communication on electronic mailing lists, source code control systems,
      and issue tracking systems that are managed by, or on behalf of, the
      Licensor for the purpose of discussing and improving the Work, but
      excluding communication that is conspicuously marked or otherwise
      designated in writing by the copyright owner as "Not a Contribution."

      "Contributor" shall mean Licensor and any individual or Legal Entity
      on behalf of whom a Contribution has been received by Licensor and
      subsequently incorporated within the Work.

   2. Grant of Copyright License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      copyright license to reproduce, prepare Derivative Works of,
      publicly display, publicly perform, sublicense, and distribute the
      Work and such Derivative Works in Source or Object form.

   3. Grant of Patent License. Subject to the terms and conditions of
      this License, each Contributor hereby grants to You a perpetual,
      worldwide, non-exclusive, no-charge, royalty-free, irrevocable
      (except as stated in this section) patent license to make, have made,
      use, offer to sell, sell, import, and otherwise transfer the Work,
      where such license applies only to those patent claims licensable
      by such Contributor that are necessarily infringed by their
      Contribution(s) alone or by combination of their Contribution(s)
      with the Work to which such Contribution(s) was submitted. If You
      institute patent litigation against any entity (including a
      cross-claim or counterclaim in a lawsuit) alleging that the Work
      or a Contribution incorporated within the Work constitutes direct
      or contributory patent infringement, then any patent licenses
      granted to You under this License for that Work shall terminate
      as of the date such litigation is filed.

   4. Redistribution. You may reproduce and distribute copies of the
      Work or Derivative Works thereof in any medium, with or without
      modifications, and in Source or Object form, provided that You
      meet the following conditions:

      (a) You must give any other recipients of the Work or
          Derivative Works a copy of this License; and

      (b) You must cause any modified files to carry prominent notices
          stating that You changed the files; and

      (c) You must retain, in the Source form of any Derivative Works
          that You distribute, all copyright, patent, trademark, and
          attribution notices from the Source form of the Work,
          excluding those notices that do not pertain to any part of
          the Derivative Works; and

      (d) If the Work includes a "NOTICE" text file as part of its
          distribution, then any Derivative Works that You distribute must
          include a readable copy of the attribution notices contained
          within such NOTICE file, excluding those notices that do not
          pertain to any part of the Derivative Works, in at least one
          of the following places: within a NOTICE text file distributed
          as part of the Derivative Works; within the Source form or
          documentation, if provided along with the Derivative Works; or,
          within a display generated by the Derivative Works, if and
          wherever such third-party notices normally appear. The contents
          of the NOTICE file are for informational purposes only and
          do not modify the License. You may add Your own attribution
          notices within Derivative Works that You distribute, alongside
          or as an addendum to the NOTICE text from the Work, provided
          that such additional attribution notices cannot be construed
          as modifying the License.

      You may add Your own copyright statement to Your modifications and
      may provide additional or different license terms and conditions
      for use, reproduction, or distribution of Your modifications, or
      for any such Derivative Works as a whole, provided Your use,
      reproduction, and distribution of the Work otherwise complies with
      the conditions stated in this License.

   5. Submission of Contributions. Unless You explicitly state otherwise,
      any Contribution intentionally submitted for inclusion in the Work
      by You to the Licensor shall be under the terms and conditions of
      this License, without any additional terms or conditions.
      Notwithstanding the above, nothing herein shall supersede or modify
      the terms of any separate license agreement you may have executed
      with Licensor regarding such Contributions.

   6. Trademarks. This License does not grant permission to use the trade
      names, trademarks, service marks, or product names of the Licensor,
      except as required for reasonable and customary use in describing the
      origin of the Work and reproducing the content of the NOTICE file.

   7. Disclaimer of Warranty. Unless required by applicable law or
      agreed to in writing, Licensor provides the Work (and each
      Contributor provides its Contributions) on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
      implied, including, without limitation, any warranties or conditions
      of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
      PARTICULAR PURPOSE. You are solely responsible for determining the
      appropriateness of using or redistributing the Work and assume any
      risks associated with Your exercise of permissions under this License.

   8. Limitation of Liability. In no event and under no legal theory,
      whether in tort (including negligence), contract, or otherwise,
      unless required by applicable law (such as deliberate and grossly
      negligent acts) or agreed to in writing, shall any Contributor be
      liable to You for damages, including any direct, indirect, special,
      incidental, or consequential damages of any character arising as a
      result of this License or out of the use or inability to use the
      Work (including but not limited to damages for loss of goodwill,
      work stoppage, computer failure or malfunction, or any and all
      other commercial damages or losses), even if such Contributor
      has been advised of the possibility of such damages.

   9. Accepting Warranty or Additional Liability. While redistributing
      the Work or Derivative Works thereof, You may choose to offer,
      and charge a fee for, acceptance of support, warranty, indemnity,
      or other liability obligations and/or rights consistent with this
      License. However, in accepting such obligations, You may act only
      on Your own behalf and on Your sole responsibility, not on behalf
      of any other Contributor, and only if You agree to indemnify,
      defend, and hold each Contributor harmless for any liability
      incurred by, or claims asserted against, such Contributor by reason
      of your accepting any such warranty or additional liability.

   END OF TERMS AND CONDITIONS

   APPENDIX: How to apply the Apache License to your work.

      To apply the Apache License to your work, attach the following
      boilerplate notice, with the fields enclosed by brackets "[]"
      replaced with your own identifying information. (Don't include
      the brackets!)  The text should be enclosed in the appropriate
      comment syntax for the file format. We also recommend that a
      file or class name and description of purpose be included on the
      same "printed page" as the copyright notice for easier
      identification within third-party archives.

   Copyright [yyyy] [name of copyright owner]

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-->
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:px="urn-x:sfti:documents:schema:xsd:HealthCareServicesSpecification-1"
   xmlns:sfticac="urn-x:sfti:schema:xsd:CommonAggregateComponents-2"
   xmlns:cac="urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2"
   xmlns:cbc="urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2"
   xmlns:sfticbc="urn-x:sfti:schema:xsd:CommonBasicComponents-2"
   xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs px sfticac cac cbc sfticbc"
   >
<!-- Start of transformation -->
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
   <xsl:template match="/px:HealthCareServicesSpecification">
      <urn1:ProcessClaimSpecification xmlns:urn2="urn:riv:financial:billing:claim:1"
         xmlns:urn1="urn:riv:financial:billing:claim:ProcessClaimSpecificationResponder:1"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="urn:riv:financial:billing:claim:ProcessClaimSpecificationResponder:1 file:../../../schemas/interactions/ProcessClaimSpecificationInteraction/ProcessClaimSpecificationResponder_1.0.xsd">
               <urn1:claimSpecification>
                  <urn2:id>
                     <xsl:value-of select="cbc:ID"/>
                  </urn2:id>
                  <xsl:if test="cbc:IssueDate">
                     <xsl:variable name="issueDate" as="xs:date" select="cbc:IssueDate"/>                     
                     <urn2:issueDate>
                     <xsl:value-of select="format-date($issueDate, '[Y0001][M01][D01]')"/>
                  </urn2:issueDate>
                  </xsl:if>
                  <xsl:if test="cbc:IssueTime">
                     <xsl:variable name="issueTime" as="xs:time" select="cbc:IssueTime"/> 
                     <urn2:issueTime>
                        <xsl:value-of select="format-time($issueTime, '[H01][m01][s01]')"/>
                     </urn2:issueTime>
                  </xsl:if>
                  <urn2:typeOfInvoice>
                     <xsl:value-of select="cbc:InvoiceTypeCode"/>
                  </urn2:typeOfInvoice>
                  <urn2:invoiceId>
                     <urn2:root>2.51.1.6</urn2:root>
                     <urn2:extension>
                        <xsl:value-of select="sfticbc:InvoiceID"/>
                     </urn2:extension>
                  </urn2:invoiceId>
                  <xsl:if test="sfticbc:InvoiceIssueDate">
                     <xsl:variable name="date" as="xs:date" select="sfticbc:InvoiceIssueDate"/> 
                     <urn2:invoiceDateOfIssue><xsl:value-of select="format-date($date, '[Y0001][M01][D01]')"/></urn2:invoiceDateOfIssue>
                  </xsl:if>
                  <xsl:if test="cbc:ReferenceToInvoice">
                     <urn2:referenceToInvoice><xsl:value-of select="cbc:ReferenceToInvoice"/></urn2:referenceToInvoice>
                  </xsl:if>
                  <xsl:if test="cbc:ReferenceToHealthCareServicesSpecificationID">
                     <urn2:referenceToHealthCareServicesSpecificationId><xsl:value-of select="cbc:ReferenceToHealthCareServicesSpecificationID"/></urn2:referenceToHealthCareServicesSpecificationId>
                  </xsl:if>
                  <urn2:payableAmount>
                     <urn2:amount>
                        <xsl:value-of select="cbc:PayableAmount"/>
                     </urn2:amount>
                     <urn2:currency>
                        <urn2:code>SEK</urn2:code>
                        <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                     </urn2:currency>
                  </urn2:payableAmount>
                  <xsl:for-each select="sfticac:Unspecified">
                     <urn2:unspecified>
                        <urn2:text>
                           <xsl:value-of select="cbc:Text"/>
                        </urn2:text>
                        <urn2:type>
                           <xsl:value-of select="cbc:TypeCode"/>
                        </urn2:type>
                     </urn2:unspecified>
                  </xsl:for-each>
                  <xsl:for-each select="cac:SupplierParty">
                     <urn2:supplierParty>
                           <urn2:identification>
                              <urn2:root><xsl:value-of select="cac:Party/cac:PartyIdentification/cbc:ID/@schemeID"/></urn2:root>
                              <urn2:extension><xsl:value-of select="cac:Party/cac:PartyIdentification/cbc:ID"/></urn2:extension>
                           </urn2:identification>
                        <xsl:for-each select="cac:SellerContact">
                           <urn2:sellerContact>
                              <urn2:name><xsl:value-of select="cbc:Name"/></urn2:name>
                              <urn2:telephone><xsl:value-of select="cbc:Telephone"/></urn2:telephone>
                              <urn2:electronicMail><xsl:value-of select="cbc:ElectronicMail"/></urn2:electronicMail>
                           </urn2:sellerContact>
                        </xsl:for-each>
                     </urn2:supplierParty>
                  </xsl:for-each>
                  
                  <xsl:for-each select="cac:CustomerParty">
                     <urn2:customerParty>
                        <urn2:identification>
                           <urn2:root><xsl:value-of select="cac:Party/cac:PartyIdentification/cbc:ID/@schemeID"/></urn2:root>
                           <urn2:extension><xsl:value-of select="cac:Party/cac:PartyIdentification/cbc:ID"/></urn2:extension>
                        </urn2:identification>
                        <xsl:for-each select="cac:BuyerContact">
                     <urn2:buyerContact>
                        <urn2:name><xsl:value-of select="cbc:Name"/></urn2:name>
                        <urn2:telephone><xsl:value-of select="cbc:Telephone"/></urn2:telephone>
                        <urn2:electronicMail><xsl:value-of select="cbc:ElectronicMail"/></urn2:electronicMail>
                     </urn2:buyerContact>
                        </xsl:for-each>
                  </urn2:customerParty>
                  </xsl:for-each>
                  <xsl:for-each select="sfticac:HealthCareServicesSpecificationLine">                     
                     <urn2:healthCareServicesSpecificationLine>
                     <xsl:for-each select="sfticac:PatientInformation">
                     <urn2:patientInformation>
                        <xsl:for-each select="sfticac:PatientIdentity">                        
                        <urn2:patientIdentity>
                           <urn2:root>1.2.752.129.2.1.3.1</urn2:root>
                           <urn2:extension><xsl:value-of select="sfticbc:PersonalID"/></urn2:extension>
                        </urn2:patientIdentity>
                        </xsl:for-each>
                        <xsl:for-each select="sfticac:PatientNameAndAddress">                           
                        <urn2:patientNameAndAddress>
                           <urn2:firstName><xsl:value-of select="sfticbc:PatientFirstName"/></urn2:firstName>
                           <urn2:lastName><xsl:value-of select="sfticbc:PatientLastName"/></urn2:lastName>
                           <xsl:for-each select="cac:PostalAddress"> 
                           <urn2:postalAddress>
                              <xsl:if test="cbc:Postbox"><urn2:postbox><xsl:value-of select="cbc:Postbox"/></urn2:postbox></xsl:if>
                              <xsl:if test="cbc:StreetName"><urn2:streetName><xsl:value-of select="cbc:StreetName"/></urn2:streetName></xsl:if>
                              <xsl:if test="cbc:CityName"><urn2:cityName><xsl:value-of select="cbc:CityName"/></urn2:cityName></xsl:if>
                              <xsl:if test="cbc:PostalZone"><urn2:postalZone><xsl:value-of select="cbc:PostalZone"/></urn2:postalZone></xsl:if>
                              <xsl:if test="cbc:IdentificationCode">
                              <urn2:country>
                                 <urn2:code><xsl:value-of select="cbc:IdentificationCode"/></urn2:code>
                                 <urn2:codeSystem>1.2.752.129.2.2.1.19</urn2:codeSystem>
                              </urn2:country>
                              </xsl:if>
                           </urn2:postalAddress>
                           </xsl:for-each>
                        </urn2:patientNameAndAddress>
                        </xsl:for-each>
                        <xsl:for-each select="sfticac:PatientOtherInformation">         
                        <urn2:patientOtherInformation>
                           <urn2:county>
                              <urn2:code><xsl:value-of select="sfticbc:CountyCode"/></urn2:code>
                              <urn2:codeSystem>1.2.752.129.2.2.1.18</urn2:codeSystem>
                           </urn2:county>
                           <urn2:municipality>
                              <urn2:code><xsl:value-of select="sfticbc:MunicipalityCode"/></urn2:code>
                              <urn2:codeSystem>1.2.752.129.2.2.1.17</urn2:codeSystem>
                           </urn2:municipality>
                           <xsl:if test="sfticbc:ListingLocalAuthoritiesNumber">
                              <urn2:listingLocalAuthoritiesNumber>
                              <urn2:code>?</urn2:code>
                              <urn2:codeSystem><xsl:value-of select="sfticbc:ListingLocalAuthoritiesNumber"/></urn2:codeSystem>
                           </urn2:listingLocalAuthoritiesNumber>
                           </xsl:if>
                           <xsl:if test="sfticbc:LMACardNumber">
                              <urn2:LMACardNumber><xsl:value-of select="sfticbc:LMACardNumber"/></urn2:LMACardNumber>
                           </xsl:if>
                           <xsl:if test="sfticbc:GenderCode">
                           <urn2:gender>
                              <urn2:code><xsl:value-of select="sfticbc:GenderCode"/></urn2:code>
                              <urn2:codeSystem>1.2.752.129.2.2.1.1</urn2:codeSystem>
                           </urn2:gender>
                           </xsl:if>
                           <xsl:if test="sfticbc:EUCardNumber"><urn2:EUCardNumber><xsl:value-of select="sfticbc:EUCardNumber"/></urn2:EUCardNumber></xsl:if>
                           <xsl:for-each select="sfticac:Unspecified">
                              <urn2:unspecified>
                                 <urn2:text>
                                    <xsl:value-of select="cbc:Text"/>
                                 </urn2:text>
                                 <urn2:type>
                                    <xsl:value-of select="cbc:TypeCode"/>
                                 </urn2:type>
                              </urn2:unspecified>
                           </xsl:for-each>
                        </urn2:patientOtherInformation>
                        </xsl:for-each>    
                     </urn2:patientInformation>
                     </xsl:for-each>
                     
                     <xsl:for-each select="sfticac:HealthCarePerformed">
                     <urn2:healthcarePerformed>
                        <urn2:lineId><xsl:value-of select="sfticbc:HealthCarePerformedLineID"/></urn2:lineId>
                        <urn2:careContactId><xsl:value-of select="sfticbc:CareContactID"/></urn2:careContactId>
                        <xsl:if test="sfticbc:ReferenceToHealthCarePerformedLineID"><urn2:referenceToLineId><xsl:value-of select="sfticbc:ReferenceToHealthCarePerformedLineID"/></urn2:referenceToLineId></xsl:if>
                        <xsl:if test="/px:HealthCareServicesSpecification/sfticac:HealthCareServicesSpecificationLine[1]/sfticac:HealthCarePerformed[2]/sfticbc:ReasonForCreditCode[1]"><urn2:reasonForCredit><xsl:value-of select="sfticbc:ReasonForCreditCode"/></urn2:reasonForCredit></xsl:if>
                        
                        <xsl:for-each select="sfticac:Unspecified">
                           <urn2:unspecified>
                              <urn2:text>
                                 <xsl:value-of select="cbc:Text"/>
                              </urn2:text>
                              <urn2:type>
                                 <xsl:value-of select="cbc:TypeCode"/>
                              </urn2:type>
                           </urn2:unspecified>
                        </xsl:for-each>
                        
                        <xsl:for-each select="sfticac:PaymentCommitment">
                        <urn2:paymentCommitment>
                           <urn2:remittanceNumber><xsl:value-of select="sfticbc:RemittanceNumber"/></urn2:remittanceNumber>
                           <urn2:paymentCommitmentNumber><xsl:value-of select="sfticbc:PaymentCommitmentNumber"/></urn2:paymentCommitmentNumber>
                           <urn2:typeOfAgreement><xsl:value-of select="sfticbc:PaymentCommitmentTypeAgreement"/></urn2:typeOfAgreement>
<!--                           <urn2:agreementInRiksavtalet><xsl:value-of select="sfticbc:AgreementInRiksavtalet"/></urn2:agreementInRiksavtalet> -->
                           <urn2:typeOfChapter><xsl:value-of select="sfticbc:PaymentCommitmentTypeOfReimbursement"/></urn2:typeOfChapter>
                           <urn2:typeOfReimbursement><xsl:value-of select="sfticbc:PaymentCommitmentTypeOfReimbursement"/></urn2:typeOfReimbursement>
                           <xsl:for-each select="sfticac:Unspecified">
                              <urn2:unspecified>
                                 <urn2:text>
                                    <xsl:value-of select="cbc:Text"/>
                                 </urn2:text>
                                 <urn2:type>
                                    <xsl:value-of select="cbc:TypeCode"/>
                                 </urn2:type>
                              </urn2:unspecified>
                           </xsl:for-each>
                        </urn2:paymentCommitment>
                        </xsl:for-each>
                        
                        <xsl:for-each select="sfticac:HealthcareCategory">
                        <urn2:healthcareCategory>
                           
                           <xsl:for-each select="sfticbc:InOutPatientCareCode">
                           <xsl:choose>
                              <xsl:when test=". = 'OV'">
                                 <urn2:typeOfCare>
                                    <urn2:code>01</urn2:code>
                                    <urn2:codeSystem>1.2.752.29.4.16</urn2:codeSystem>
                                 </urn2:typeOfCare>
                              </xsl:when>
                              <xsl:when test=". = 'SV'">
                                 <urn2:typeOfCare>
                                    <urn2:code>02</urn2:code>
                                    <urn2:codeSystem>1.2.752.29.4.16</urn2:codeSystem>
                                 </urn2:typeOfCare>
                              </xsl:when>
                           </xsl:choose>
                           </xsl:for-each>
                           
                           <xsl:for-each select="sfticbc:PublicPrivateCareCode">
                           <xsl:choose>
                              <xsl:when test=". = 'P'">
                                 <urn2:publicCare>false</urn2:publicCare></xsl:when>
                              <xsl:otherwise>
                                 <urn2:publicCare>true</urn2:publicCare>
                              </xsl:otherwise>
                           </xsl:choose>
                           </xsl:for-each>
                           
                           <xsl:for-each select="sfticbc:NotPlannedHealthCare">
                           <xsl:choose>
                              <xsl:when test=". = 2">
                                 <urn2:plannedHealthcare>true</urn2:plannedHealthcare>                                 
                              </xsl:when>
                              <xsl:otherwise>
                                 <urn2:plannedHealthcare>false</urn2:plannedHealthcare>
                              </xsl:otherwise>
                           </xsl:choose>
                           </xsl:for-each>
                           
                           <xsl:if test="sfticbc:FirstReVisitCode"><urn2:typeOfVisit><xsl:value-of select="sfticbc:FirstReVisitCode"/></urn2:typeOfVisit></xsl:if>
                           <xsl:for-each select="sfticac:Unspecified">
                              <urn2:unspecified>
                                 <urn2:text>
                                    <xsl:value-of select="cbc:Text"/>
                                 </urn2:text>
                                 <urn2:type>
                                    <xsl:value-of select="cbc:TypeCode"/>
                                 </urn2:type>
                              </urn2:unspecified>
                           </xsl:for-each>
                        </urn2:healthcareCategory>
                        </xsl:for-each>
                        <xsl:for-each select="sfticac:TimeSpan">
                        <urn2:timespan>
                           <xsl:for-each select="sfticac:HealthCareVisitDateTime">
                              <urn2:healthcareVisitDateTime>
                                 
                              <xsl:if test="sfticbc:CareVisitDate">
                                 <xsl:variable name="date" as="xs:date" select="sfticbc:CareVisitDate"/>                     
                                 <urn2:careVisitDate><xsl:value-of select="format-date($date, '[Y0001][M01][D01]')"/></urn2:careVisitDate>
                              </xsl:if>
                                 <xsl:if test="sfticbc:CareVisitTime">
                                    <xsl:variable name="time" as="xs:time" select="sfticbc:CareVisitTime"/>  
                                    <urn2:careVisitTime><xsl:value-of select="format-time($time, '[H01][m01][s01]')"/></urn2:careVisitTime>
                                 </xsl:if>
                              </urn2:healthcareVisitDateTime>
                           </xsl:for-each>
                           <xsl:for-each select="sfticac:HealthCareDischargeDateTime">
                           <urn2:healthcareDischargeDateTime>
                              <xsl:variable name="date" as="xs:date" select="sfticbc:CareDischargeDate"/> 
                              <urn2:careDischargeDate><xsl:value-of select="format-date($date, '[Y0001][M01][D01]')"/></urn2:careDischargeDate>
                              <xsl:if test="sfticbc:CareDischargeTime">
                              <xsl:variable name="time" as="xs:time" select="sfticbc:CareDischargeTime"/> 
                              <urn2:careDischargeTime><xsl:value-of select="format-time($time, '[H01][m01][s01]')"/></urn2:careDischargeTime>
                              </xsl:if>
                           </urn2:healthcareDischargeDateTime>                    
                           </xsl:for-each>
                           <xsl:for-each select="cac:InvoicePeriod">
                              <urn2:invoicePeriod>
                              <urn2:start>?</urn2:start>
                              <urn2:end>?</urn2:end>
                           </urn2:invoicePeriod>           
                           </xsl:for-each>
                           <xsl:for-each select="sfticac:NumberOfDays">
                              <urn2:numberOfDays>
                                 <xsl:if test="sfticbc:NumberOfDaysOnLeave"><urn2:onLeave><xsl:value-of select="sfticbc:NumberOfDaysOnLeave"/></urn2:onLeave></xsl:if>
                                 <xsl:if test="sfticbc:NumberOfDaysInCare"><urn2:inCare><xsl:value-of select="sfticbc:NumberOfDaysInCare"/></urn2:inCare></xsl:if>
                           </urn2:numberOfDays>
                           </xsl:for-each>
                        </urn2:timespan>
                        </xsl:for-each>
                        <xsl:for-each select="sfticac:HealthCareUnit">
                        <urn2:healthcareUnit>
                           <xsl:if test="sfticbc:CareUnitID">
                           <urn2:careUnitId>
                              <urn2:root>1.2.752.129.2.1.4.1</urn2:root>
                              <urn2:extension><xsl:value-of select="sfticbc:CareUnitID"/></urn2:extension>
                           </urn2:careUnitId>
                           </xsl:if>
                           <urn2:careUnitName><xsl:value-of select="sfticbc:CareUnitName"/></urn2:careUnitName>
                           <xsl:if test="sfticbc:ProfessionCodeForHealthCare">
                              <urn2:professionCodeForHealthcare><xsl:value-of select="sfticbc:ProfessionCodeForHealthCare"/></urn2:professionCodeForHealthcare>
                           </xsl:if>
                           <xsl:for-each select="sfticac:Unspecified">
                              <urn2:unspecified>
                                 <urn2:text>
                                    <xsl:value-of select="cbc:Text"/>
                                 </urn2:text>
                                 <urn2:type>
                                    <xsl:value-of select="cbc:TypeCode"/>
                                 </urn2:type>
                              </urn2:unspecified>
                           </xsl:for-each>
                        </urn2:healthcareUnit>
                        </xsl:for-each>
                        <xsl:for-each select="sfticac:Categorization">
                        <urn2:categorization>
                           <xsl:if test="sfticbc:ProductCategoryCode"><urn2:productCategory><xsl:value-of select="sfticbc:ProductCategoryCode"/></urn2:productCategory></xsl:if>
                           <xsl:if test="sfticbc:PriceListCode"><urn2:priceList><xsl:value-of select="sfticbc:PriceListCode"/></urn2:priceList></xsl:if>
                           <xsl:for-each select="sfticac:DRGCost">
                           <urn2:DRGCost>
                              <urn2:DRGCode>
                                 <urn2:originalText><xsl:value-of select="sfticbc:DRGCode"/></urn2:originalText>
                              </urn2:DRGCode>
                              <urn2:DRGPrice>
                              <urn2:amount><xsl:value-of select="sfticbc:DRGPrice"/></urn2:amount>
                              <urn2:currency>
                                 <urn2:code>SEK</urn2:code>
                                 <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                              </urn2:currency>
                              </urn2:DRGPrice>
                              <xsl:if test="sfticbc:ExtendedAllowanceCareTime"><urn2:extendedAllowanceCareTime>
                                 <urn2:amount><xsl:value-of select="sfticbc:ExtendedAllowanceCareTime"/></urn2:amount>
                                 <urn2:currency>
                                    <urn2:code>SEK</urn2:code>
                                    <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                                 </urn2:currency></urn2:extendedAllowanceCareTime></xsl:if>
                              <xsl:if test="sfticbc:ExtendedAllowanceCost"><urn2:extendedAllowanceCost>
                                 <urn2:amount><xsl:value-of select="sfticbc:ExtendedAllowanceCost"/></urn2:amount>
                                 <urn2:currency>
                                    <urn2:code>SEK</urn2:code>
                                    <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                                 </urn2:currency></urn2:extendedAllowanceCost></xsl:if>
                           </urn2:DRGCost>
                           </xsl:for-each>
                           <xsl:for-each select="sfticac:PatientSpecificCare">
                           <urn2:patientSpecificCare>
                              <urn2:patientSpecificCareCode><xsl:value-of select="sfticbc:PatientSpecificCareCode"/></urn2:patientSpecificCareCode>
                              <urn2:patientSpecificCareCodeName><xsl:value-of select="sfticbc:PatientSpecificCareCodeName"/></urn2:patientSpecificCareCodeName>
                              <urn2:numberOfPatientSpecificCare><xsl:value-of select="sfticbc:NumberOfPatientSpecificCare"/></urn2:numberOfPatientSpecificCare>
                              <urn2:patientSpecificCarePrice>
                                 <urn2:amount><xsl:value-of select="sfticbc:PatientSpecificCarePrice"/></urn2:amount>
                                 <urn2:currency>
                                    <urn2:code>SEK</urn2:code>
                                    <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                                 </urn2:currency>
                              </urn2:patientSpecificCarePrice>
                              <urn2:patientSpecificCareTotalAmount>
                                 <urn2:amount><xsl:value-of select="sfticbc:PatientSpecificCareTotalAmount"/></urn2:amount>
                                 <urn2:currency>
                                    <urn2:code>SEK</urn2:code>
                                    <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                                 </urn2:currency>
                              </urn2:patientSpecificCareTotalAmount>
                           </urn2:patientSpecificCare>
                           </xsl:for-each>
                        </urn2:categorization>
                        </xsl:for-each>
                        <xsl:for-each select="sfticac:InvoicedAmountDetails">
                        <urn2:invoicedAmountDetails>
                           <xsl:for-each select="sfticbc:PatientCareInvoiceGrossAmount">
                           <urn2:patientCareInvoiceGrossAmount>
                              <urn2:amount><xsl:value-of select="."/></urn2:amount>
                              <urn2:currency>
                                 <urn2:code>SEK</urn2:code>
                                 <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                              </urn2:currency>
                           </urn2:patientCareInvoiceGrossAmount>
                           </xsl:for-each>
                           <xsl:for-each select="sfticbc:PatientCareInvoiceNetAmount">
                           <urn2:patientCareInvoiceNetAmount>
                              <urn2:amount><xsl:value-of select="."/></urn2:amount>
                              <urn2:currency>
                                 <urn2:code>SEK</urn2:code>
                                 <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                              </urn2:currency>
                           </urn2:patientCareInvoiceNetAmount>
                           </xsl:for-each>
                           <xsl:for-each select="sfticbc:PatientCareAllowance">
                           <urn2:patientCareAllowance>
                              <urn2:amount><xsl:value-of select="."/></urn2:amount>
                              <urn2:currency>
                                 <urn2:code>SEK</urn2:code>
                                 <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                              </urn2:currency>
                           </urn2:patientCareAllowance>
                           </xsl:for-each>
                           <xsl:for-each select="sfticbc:PatientCareDiscount">
                           <urn2:patientCareDiscount>
                              <urn2:amount><xsl:value-of select="."/></urn2:amount>
                              <urn2:currency>
                                 <urn2:code>SEK</urn2:code>
                                 <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                              </urn2:currency>
                           </urn2:patientCareDiscount>
                           </xsl:for-each>   
                           <xsl:for-each select="sfticbc:PatientCareCharge">
                           <urn2:patientCareCharge>
                              <urn2:amount><xsl:value-of select="."/></urn2:amount>
                              <urn2:currency>
                                 <urn2:code>SEK</urn2:code>
                                 <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                              </urn2:currency>
                           </urn2:patientCareCharge>
                           </xsl:for-each>
                           <xsl:for-each select="sfticbc:OutPatientCareFee">
                           <urn2:outPatientCareFee>
                              <urn2:amount><xsl:value-of select="."/></urn2:amount>
                              <urn2:currency>
                                 <urn2:code>SEK</urn2:code>
                                 <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                              </urn2:currency>
                           </urn2:outPatientCareFee>
                           </xsl:for-each>
                           <xsl:for-each select="sfticbc:OutPatientCareFeePaid">
                              <urn2:outPatientCareFeePaid>
                                 <urn2:amount><xsl:value-of select="."/></urn2:amount>
                                 <urn2:currency>
                                    <urn2:code>SEK</urn2:code>
                                    <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                                 </urn2:currency>
                              </urn2:outPatientCareFeePaid>
                           </xsl:for-each>
                           <xsl:for-each select="sfticbc:PatientFeeReducedCategory">
                           <urn2:patientFeeReducedCategory>
                              <urn2:amount><xsl:value-of select="."/></urn2:amount>
                              <urn2:currency>
                                 <urn2:code>SEK</urn2:code>
                                 <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                              </urn2:currency>
                           </urn2:patientFeeReducedCategory>
                           </xsl:for-each>
                           <xsl:if test="sfticbc:PatientFreePassCauseIndicatorType"><urn2:patientFreePassCauseIndicator><xsl:value-of select="sfticbc:PatientFreePassCauseIndicatorType"/></urn2:patientFreePassCauseIndicator></xsl:if>
                           <xsl:for-each select="sfticbc:InpatientCareFee">
                           <urn2:inpatientCareFee>
                              <urn2:amount><xsl:value-of select="."/></urn2:amount>
                              <urn2:currency>
                                 <urn2:code>SEK</urn2:code>
                                 <urn2:codeSystem>1.0.4217</urn2:codeSystem>
                              </urn2:currency>
                           </urn2:inpatientCareFee>
                           </xsl:for-each>
                           <xsl:for-each select="sfticac:Unspecified">
                              <urn2:unspecified>
                                 <urn2:text>
                                    <xsl:value-of select="cbc:Text"/>
                                 </urn2:text>
                                 <urn2:type>
                                    <xsl:value-of select="cbc:TypeCode"/>
                                 </urn2:type>
                              </urn2:unspecified>
                           </xsl:for-each>
                        </urn2:invoicedAmountDetails>
                        </xsl:for-each>
                        <xsl:for-each select="sfticac:Activity">
                        <urn2:activity>
                           <xsl:for-each select="sfticbc:MedicalFieldCode"><urn2:medicalFieldCode><xsl:value-of select="."/></urn2:medicalFieldCode></xsl:for-each>
                           <xsl:for-each select="sfticbc:HSAActivityCode"><urn2:businessClassification>
                              <urn2:code><xsl:value-of select="."/></urn2:code>
                              <urn2:codeSystem>1.2.752.29.4.1</urn2:codeSystem></urn2:businessClassification></xsl:for-each>
                           <xsl:for-each select="sfticbc:CareLevelCode"><urn2:careLevel><xsl:value-of select="."/></urn2:careLevel></xsl:for-each>
                           <xsl:for-each select="sfticac:Unspecified">
                              <urn2:unspecified>
                                 <urn2:text>
                                    <xsl:value-of select="cbc:Text"/>
                                 </urn2:text>
                                 <urn2:type>
                                    <xsl:value-of select="cbc:TypeCode"/>
                                 </urn2:type>
                              </urn2:unspecified>
                           </xsl:for-each>
                        </urn2:activity>
                        </xsl:for-each>
                        <xsl:for-each select="sfticac:Diagnosis">
                        <urn2:diagnosis>
                           <xsl:for-each select="sfticac:InPatientCareDiagnosis">
                           <urn2:inpatientCareDiagnosis>
                              <xsl:for-each select="sfticac:MainDiagnosis">
                              <urn2:mainDiagnosis>
                                 <urn2:diagnosisCode>
                                    <urn2:code><xsl:value-of select="sfticbc:DiagnosisCode"/></urn2:code>
                                    <urn2:codeSystem>1.2.752.116.1.1.1.1.3</urn2:codeSystem>
                                 </urn2:diagnosisCode>
                              </urn2:mainDiagnosis>
                           </xsl:for-each>
                              <xsl:for-each select="sfticac:BiDiagnosis">
                              <urn2:biDiagnosis>
                                 <urn2:diagnosisCode>
                                    <urn2:code>1.2.752.116.1.1.1.1.3</urn2:code>
                                    <urn2:codeSystem><xsl:value-of select="sfticbc:DiagnosisCode"/></urn2:codeSystem>
                                 </urn2:diagnosisCode>
                              </urn2:biDiagnosis>
                           </xsl:for-each>
                           </urn2:inpatientCareDiagnosis>
                           </xsl:for-each>
                           <xsl:for-each select="sfticac:OutPatientCareDiagnosis">
                           <urn2:outpatientCareDiagnosis>
                              <xsl:for-each select="sfticac:MainDiagnosis">
                              <urn2:mainDiagnosis>
                                 <urn2:diagnosisCode>
                                    <urn2:code>1.2.752.116.1.1.1.1.3</urn2:code>
                                    <urn2:codeSystem><xsl:value-of select="sfticbc:DiagnosisCode"/></urn2:codeSystem>
                                 </urn2:diagnosisCode>
                              </urn2:mainDiagnosis>
                              </xsl:for-each>
                              <xsl:for-each select="sfticac:BiDiagnosis">
                              <urn2:biDiagnosis>
                                 <urn2:diagnosisCode>
                                    <urn2:code>1.2.752.116.1.1.1.1.3</urn2:code>
                                    <urn2:codeSystem><xsl:value-of select="sfticbc:DiagnosisCode"/></urn2:codeSystem>
                                 </urn2:diagnosisCode>
                              </urn2:biDiagnosis>
                              </xsl:for-each>
                           </urn2:outpatientCareDiagnosis>
                           </xsl:for-each>
                        </urn2:diagnosis>
                        </xsl:for-each>
                        <xsl:for-each select="sfticac:Treatment">
                        <urn2:treatment>
                           <xsl:for-each select="sfticbc:TreatmentCode">
                           <urn2:typeOfTreatment>
                              <urn2:code><xsl:value-of select="."/></urn2:code>
                              <urn2:codeSystem>1.2.752.116.1.3.2.1.4</urn2:codeSystem>
                           </urn2:typeOfTreatment>   
                           </xsl:for-each>
                           <xsl:for-each select="sfticbc:ATCCode">
                           <urn2:ATC>
                              <urn2:code>?</urn2:code>
                              <urn2:codeSystem>?</urn2:codeSystem>
                           </urn2:ATC>
                           </xsl:for-each>
                           <xsl:for-each select="sfticac:Unspecified">
                              <urn2:unspecified>
                                 <urn2:text>
                                    <xsl:value-of select="cbc:Text"/>
                                 </urn2:text>
                                 <urn2:type>
                                    <xsl:value-of select="cbc:TypeCode"/>
                                 </urn2:type>
                              </urn2:unspecified>
                           </xsl:for-each>
                        </urn2:treatment>   
                        </xsl:for-each>
                     </urn2:healthcarePerformed>
                     </xsl:for-each>
                  </urn2:healthCareServicesSpecificationLine>
                  </xsl:for-each>
               </urn1:claimSpecification>
      </urn1:ProcessClaimSpecification>
<!-- -->
    </xsl:template>
</xsl:stylesheet>
