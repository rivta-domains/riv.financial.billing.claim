import com.helger.schematron.pure.SchematronResourcePure;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.oclc.purl.dsdl.svrl.FailedAssert;
import org.oclc.purl.dsdl.svrl.SchematronOutputType;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Author:  Khaled Daham
 */
@RunWith(JUnitPlatform.class)
public class SchematronConstraintTest {

    String FILE_CONSTRAINTS_PATTERN = "constraints.xml";
    String FILE_NEGATIVE_INPUT = "test/negative";
    String FILE_POSITIVE_INPUT = "test/positive";

    @TestFactory
    Stream<DynamicTest> validate_schematron_constraints() throws IOException {
        return DynamicTest.stream(
                    findInteractionsWithConstraints().iterator(),
                    interaction -> "Testing " + interaction.getParent().getFileName(),
                    interaction -> testInteraction(interaction)
        );
    }

    private void testInteraction(Path i) {
        System.out.println("Validating with " + i.toString());
        SchematronResourcePure resource = SchematronValidationHandler(i.toString());
        runPositiveTests(resource, i.getParent().resolve(FILE_POSITIVE_INPUT));
        runNegativeTests_assert_id_equals_filename(resource, i.getParent().resolve(FILE_NEGATIVE_INPUT));
    }

    void runNegativeTests_assert_id_equals_filename(SchematronResourcePure resource, Path root) {
        System.out.println("Negative tests from: " + root.toString());
        try {
            Files.walk(root)
                .filter(Files::isRegularFile).filter(p -> p.toString().endsWith(".xml"))
                .forEach(file -> {
                    List<FailedAssert> asserts = validate(resource, file);
                    assertNotNull(asserts);
                    AtomicInteger expectedFailedAsserts = new AtomicInteger();
                    asserts.forEach(failed -> {
                        assertEquals(filenameWithoutExtension(file), failed.getId(), assertsToString(asserts));
                        expectedFailedAsserts.getAndIncrement();
                    });
                    if(expectedFailedAsserts.get() == 0) {
                        expectedFailedAsserts.set(1);
                    }
                    //System.out.println(assertsToString(asserts));
                    assertEquals(expectedFailedAsserts.get(), asserts.size(), assertsToString(asserts));
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void runPositiveTests(SchematronResourcePure resource, Path root) {
        System.out.println("Positive tests from: " + root.toString());
        try {
            Files.walk(root)
                .filter(Files::isRegularFile).filter(p -> p.toString().endsWith(".xml"))
                .forEach(file -> {
                    List<FailedAssert> asserts = validate(resource, file);
                    assertNotNull(asserts);
                    assertEquals(0, asserts.size(), assertsToString(asserts));
                });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<FailedAssert> validate(SchematronResourcePure resource, Path file) {
        System.out.println("\t validating against: " + file.getFileName());
        Source source = new StreamSource(file.toFile());
        SchematronOutputType svrl = null;
        try {
            svrl = resource.applySchematronValidationToSVRL(source);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(svrl.hasActivePatternAndFiredRuleAndFailedAssertEntries());
        return svrl.getActivePatternAndFiredRuleAndFailedAssert()
            .stream()
            .filter(x -> x instanceof FailedAssert)
            .map(x -> (FailedAssert)x)
            .collect(Collectors.toList());
    }

    String filenameWithoutExtension(Path file) {
        String fileName = file.getFileName().toString();
        return fileName.substring(0, fileName.lastIndexOf("."));
    }

    SchematronResourcePure SchematronValidationHandler(String schematron) {
        SchematronResourcePure resource = SchematronResourcePure.fromClassPath(schematron);
        if (!resource.isValidSchematron()) {
            throw new IllegalArgumentException("Invalid Schematron!");
        }
        return resource;
    }

    Stream<Path> findInteractionsWithConstraints() throws IOException {
        Path start = Paths.get(System.getenv("PWD"));
        Stream<Path> result = Files.find(start,
                    Integer.MAX_VALUE,
                    (path, basicFileAttributes) -> path.toFile().getName().matches(FILE_CONSTRAINTS_PATTERN)
        );
        return result;
    }

    private String assertsToString(List<FailedAssert> asserts) {
        String comment = "";
        for(FailedAssert failed : asserts){
            String errorText = failed.getText().replace("\n", " ").replaceAll("\\s+", " ");
            comment += "Error: " + errorText +
                        "\nLocation: " + failed.getLocation() +
                        "\nTest[Id: " + failed.getId() + "]: " + failed.getTest() + "\n\n";
        }
        return comment;
    }
}
